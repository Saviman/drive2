'use strict';


window.onload = function() {

   var nodesComment = {
      full: [],
      text: document.querySelectorAll('.comment__text > p'),
      wrap: document.querySelector('.wrap__feed'),
      moreText: 'Читать полностью',
      maxLength: 200,
      openFullComment: function(id) {
         this.text[id].innerHTML = this.full[id];
      }
   }

   nodesComment.text.forEach( function(el, i) {

      var text = el.innerText;
      nodesComment.full[i] = text;

      if ( text.length > nodesComment.maxLength ) {
         el.innerHTML = text.slice(0, nodesComment.maxLength) + '...' + ' <a class="c-article-card__more-link c-link" data-comentid="' + i + '">' + nodesComment.moreText + '</a>';
      }
   });

   nodesComment.wrap.onclick = function(event) {
      var target = event.target;

      if (target.hasAttribute('data-comentid')) {
         var attr = target.getAttribute('data-comentid');
         nodesComment.openFullComment(attr);
      }

   };


   document.body.onclick = function(event) {

      if ( !event.target.closest('.comment-add__wrap') ) {

         var nodes = {
            openForm: this.querySelectorAll('.comment-add__title_edit'),
            commentBody: this.querySelectorAll('.comment-add__body_visible')
         }

         if ( nodes.openForm.length != 0 && nodes.commentBody.length != 0 ) {

            nodes.openForm.forEach( function(el) {
               el.classList.remove('comment-add__title_edit');
            });

            nodes.commentBody.forEach( function(el) {
               el.classList.remove('comment-add__body_visible');
            });

         }
      }

   };



   [].forEach.call( document.querySelectorAll('.comment-add__header'), function(el) {
      el.onfocus = function() {
         openForm(this);
      };

      el.addEventListener('click', function() {
         openForm(this);
      }, false);
   });

};

function openForm(el) {

   var commentBody = el.nextElementSibling;

   if ( !commentBody.classList.contains('comment-add__body_visible') ) {
      el.lastElementChild.classList.add('comment-add__title_edit');
      commentBody.classList.add('comment-add__body_visible');
      commentBody.querySelector('textarea').focus();
   }

   customValidity( commentBody.querySelector('textarea') );
}

function customValidity(el) {

   el.addEventListener('invalid', function(el) {
      el.target.setCustomValidity('');
      if ( !el.target.validity.valid ) {
         el.target.setCustomValidity('Пожалуйста, введите комментарий.');
      }
   });

}
